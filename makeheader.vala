/*******************************************/
/*                   WildCard              */
/*******************************************/
string []wildcard (string path, FileTest test) {
	string []result = {};
	try {
		string? filename;
		var path_open = Path.get_dirname (path) + "/";
		var patern = Path.get_basename (path);
		Dir dir = Dir.open (path_open);
		var regex = new Regex (patern.replace(".", "[.]").replace("*", ".*"), OPTIMIZE);

		while ((filename = dir.read_name ()) != null) {
			string new_path = path_open + filename;
			if (FileUtils.test (new_path, test))
				if (regex.match (filename))
					result += new_path;
		}
	}
	catch (Error e) {
		printerr("Error: %s\n", e.message);
	}
	return result;
}

void recurse (StrvBuilder strv, string path, int i = 0) {
	while (true) {
		i = path.index_of_char ('/', i + 1);
		if (i == -1) {
			if ("*" in path) {
				var files = wildcard (path, FileTest.IS_REGULAR);
				foreach (unowned string file in files) {
					strv.add (file);
				}
			}
			if (FileUtils.test (path, FileTest.IS_REGULAR))
				strv.add (path);
			return ;
		}
		var current_dir = path[0:i];
		if ("*" in Path.get_basename (current_dir)) {
			var dirs = wildcard (current_dir, FileTest.IS_DIR);
			foreach (unowned string dir in dirs) {
				recurse (strv, dir + path[i:], i);
			}
			return ;
		}
	}
}



string []get_all_prototype_from_file (string filename) throws Error {
	string content;
	FileUtils.get_contents (filename, out content);
	return get_all_prototype (content);
}

string []get_all_prototype (string txt) throws Error {
	string []all_lines = {};
	MatchInfo info;

	var regex = /\t{2,}/;
	if (regex_prototype_multi.match(txt, 0, out info)){
		do {
			var line = info.fetch(0);
			line = line.replace ("\n", "");
			line = line.replace ("{", "");
			line = regex.replace(line, -1, 0, "	");
			line = line.replace("\t", " ");
            if (("main" in line) || ("static" in line)) {
				continue;
			}
			all_lines += line;
		} while (info.next());
	}
	return all_lines;
}

public string get_file_content(string filestream) throws Error {
	if (filestream == "@@@stdin@@@") {
		var builder = new StringBuilder.sized(65536);
		uint8 buffer[8193];
		size_t len;

		while ((len = stdin.read(buffer[0:8192])) > 0) {
			buffer[len] = '\0';
			builder.append((string)buffer);
		}
		return (owned)builder.str;
	}
	else {
		string contents;
		FileUtils.get_contents (filestream, out contents);
		return (owned)contents;
	}
}


/*******************************************/
/*                   MODE                  */
/*******************************************/

void main_format_mode () throws Error {
	var regex_instruct = new Regex("""[/][/]\s*?(?P<files>(.*))""");
	StrvBuilder? prototypes = null;
	MatchInfo info;

	var contents= get_file_content("@@@stdin@@@");
	var lines = contents.split ("\n");
	foreach (unowned var line in lines[0:lines.length - 1]) {
		if (regex_instruct.match(line, 0, out info)) {
			var files = info.fetch_named ("files").split(" ");
			prototypes = new StrvBuilder();
			foreach (unowned var i in files) {
				if (i == "")
					continue;
				var all_files = new StrvBuilder();
				recurse (all_files, i);
				foreach (unowned var y in all_files.end()){
					if (y.has_suffix (".c"))
						prototypes.addv(get_all_prototype_from_file (y));
				}
			}
		}
		if (!regex_prototype.match(line))
			print("%s\n", line);
		if (prototypes != null) {
			foreach (var i in prototypes.end()) {
				print("%s;\n", i);
			}
			prototypes = null;
		}
	}
	return;
}

void main_normal_mode (string []args) throws Error {
    var builder = new StringBuilder.sized(65536);
	foreach (unowned var av in args[1:args.length]) {
		builder.append(get_file_content (av));
	}
	// Get functions
	var lines = get_all_prototype (builder.str);
	foreach (unowned var i in lines) {
		print("%s;\n", i);
	}
}

public Regex regex_prototype;
public Regex regex_prototype_multi;

void main (string []args)
{
    try {
		regex_prototype = /^((struct|enum|union|unsigned|signed)\s)?(long\s)?(long\s)?[a-zA-Z0-9_]+[*]?\s*[a-zA-Z_0-9_*]+\s*[(].*[)]/;
		regex_prototype_multi = new Regex("""^((struct|enum|union|unsigned|signed)\s)
		?(long\s)?(long\s)?[a-zA-Z0-9_]+[*]?\s*
		[a-zA-Z_0-9*]+\s*
		[(][^{;]+[{]""",
	MULTILINE | EXTENDED);

		if (args[1] == "--format")
			main_format_mode ();
		else
			main_normal_mode (args);
    } catch (Error e){
        printerr(e.message);
    }
}
