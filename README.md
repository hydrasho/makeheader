MakeHeader is a program for generating headers.

## How Compile it ?

```bash
valac makeheader.vala
```

## How Install it ?

### for supravim users

```bash
suprapack add plugin-makeheader
```

### for others users

add makeheader in your PATH and copy plugin folder content in $HOME/.vim/plugin

```bash
mkdir -p $HOME/.vim/plugin
mv plugin/* $HOME/.vim/plugin
```


## How use

### without vim
```bash
makeheader your_file.c other_file.c
# the output is all prototypes of your_file.c and other_file.c
```

### with vim

you are in the hello.h file

press <F9> and makeheader print all prototypes of hello.c
If you want, you can specify the files to be added with this comment:

```c
// make-header file.c

(this syntax work too)
// makeheader file.c
// header file.c
```

<img src="makeheader.gif">
